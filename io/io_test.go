package io

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

// A test file
type test struct {
	file, desc                           string
	numVertices, numFaces, oppositeCount int
}

func TestReadPly(t *testing.T) {

	testFiles := []test{
		{"testdata/quad.ply", "ASCII ply mesh", 4, 2, 2},
		{"testdata/quad_b.ply", "Binary ply mesh", 4, 2, 2},
		{"testdata/quad_n.ply", "ASCII ply mesh with per-vertex normals", 4, 2, 2},
		{"testdata/quad_n_b.ply", "Binary ply mesh with per-vertex normals", 4, 2, 2},
		{"testdata/pyramid.ply", "ASCII ply mesh", 5, 6, 18},
		{"testdata/pyramid_b.ply", "Binary ply mesh", 5, 6, 18},
		{"testdata/pyramid_color.ply", "ASCII ply mesh with vertex colors", 5, 6, 18},
		{"testdata/pyramid_color_b.ply", "Binary ply mesh with vertex colors", 5, 6, 18},
		{"testdata/pyramid_color_n.ply", "ASCII ply mesh with vertex colors and per-vertex normals", 5, 6, 18},
		{"testdata/pyramid_color_n_b.ply", "Binary ply mesh with vertex colors and per-vertex normals", 5, 6, 18},
		{"testdata/cube.ply", "ASCII ply mesh", 8, 6, 24},
	}

	for _, test := range testFiles {
		Convey("Given an "+test.desc, t, func() {
			mesh, err := ReadMesh(test.file)
			Convey("It should correctly parse it as a mesh", func() {
				So(err, ShouldBeNil)
				So(mesh.Vertices, ShouldHaveLength, test.numVertices)
				So(mesh.Faces, ShouldHaveLength, test.numFaces)

				var oppositeCount = 0

				// Check halfedge connections
				for i := 0; i < len(mesh.Faces); i++ {
					start := mesh.Faces[i].HalfEdge
					it := mesh.Faces[i].HalfEdge
					for next := uint8(0); next < mesh.Faces[i].VerticesCount; next++ {
						it = it.Next
					}
					So(it, ShouldEqual, start)
				}

				for i := 0; i < len(mesh.Faces); i++ {
					it := mesh.Faces[i].HalfEdge

					for j := uint8(0); j < mesh.Faces[i].VerticesCount; j++ {
						if it.Opposite != nil {
							oppositeCount++
						}
						it = it.Next
					}
				}

				So(oppositeCount, ShouldEqual, test.oppositeCount)
			})
		})
	}
}
