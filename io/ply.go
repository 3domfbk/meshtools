package io

import (
	"bufio"
	"encoding/binary"
	"math"
	"os"

	"strings"

	"strconv"

	"bitbucket.org/3domfbk/meshtools/types"
)

// Ply
const (
	LitDouble = "double"
	LitFloat  = "float"

	LitR            = "r"
	LitRed          = "red"
	LitDiffuseRed   = "diffuse_red"
	LitG            = "g"
	LitGreen        = "green"
	LitDiffuseGreen = "diffuse_green"
	LitB            = "b"
	LitBlue         = "blue"
	LitDiffuseBlue  = "diffuse_blue"
)

// Ply element
type element struct {
	name, typename string
}

// readPly reads a mesh from a .ply file
func readPly(filename string) (*types.Mesh, error) {

	// Open file
	f, err := os.Open(filename)
	defer f.Close()

	if err != nil {
		return nil, err
	}

	// Read header
	var readingElement string
	var numVertices int64
	var numFaces int64

	elements := make([]element, 0, 10)
	ascii := true
	headerBytes := 0
	hasNormals, hasColors := false, false
	var endianity binary.ByteOrder

	reader := bufio.NewReader(f)
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		headerBytes += len(line) + 1
		values := strings.Fields(line)
		endHeader := false

		switch values[0] {
		case "end_header":
			endHeader = true
		case "format":
			switch values[1] {
			case "binary_little_endian":
				ascii = false
				endianity = binary.LittleEndian
			case "binary_big_endian":
				ascii = false
				endianity = binary.BigEndian
			}
		case "element":
			readingElement = values[1]
			switch readingElement {
			case "vertex":
				numVertices, err = strconv.ParseInt(values[2], 10, 64)
				if err != nil {
					return nil, err
				}
			case "face":
				numFaces, err = strconv.ParseInt(values[2], 10, 64)
				if err != nil {
					return nil, err
				}
			}

		case "property":
			switch readingElement {
			case "vertex":

				elements = append(elements, element{name: values[2], typename: values[1]})

				if values[2] == "nx" {
					hasNormals = true
				}

				if values[2] == LitR || values[2] == LitRed || values[2] == LitDiffuseRed {
					hasColors = true
				}
			}
		}
		// Header end
		if endHeader {
			break
		}
	}

	// Create mesh
	mesh := types.NewMesh(numVertices, numFaces, hasNormals, hasColors)

	if ascii {
		err = readPlyASCII(mesh, scanner, numVertices, numFaces, elements)
	} else {
		f.Close()
		f, err = os.Open(filename)
		if err != nil {
			return nil, err
		}
		reader = bufio.NewReader(f)
		reader.Discard(headerBytes)
		err = readPlyBinary(mesh, reader, numVertices, numFaces, endianity, elements)
	}

	return mesh, err
}

// read ascii ply
func readPlyASCII(mesh *types.Mesh, scanner *bufio.Scanner, numVertices int64, numFaces int64, elements []element) error {
	var err error
	var readingFaces = false
	var halfEdgesMap = make(map[uint64]*types.HalfEdge)

	// Read vertices
	for scanner.Scan() {
		var vertex types.Vertex
		vertex.Nx, vertex.Ny, vertex.Nz = -2, -2, -2
		values := strings.Fields(scanner.Text())

		if !readingFaces {
			for i := 0; i < len(elements); i++ {
				e := elements[i]
				switch e.name {
				case "x":
					vertex.X, err = strconv.ParseFloat(values[i], 64)
				case "y":
					vertex.Y, err = strconv.ParseFloat(values[i], 64)
				case "z":
					vertex.Z, err = strconv.ParseFloat(values[i], 64)
				case "nx":
					var v float64
					v, err = strconv.ParseFloat(values[i], 32)
					if err == nil {
						vertex.Nx = float32(v)
					}
				case "ny":
					var v float64
					v, err = strconv.ParseFloat(values[i], 32)
					if err == nil {
						vertex.Ny = float32(v)
					}
				case "nz":
					var v float64
					v, err = strconv.ParseFloat(values[i], 32)
					if err == nil {
						vertex.Nz = float32(v)
					}
				case LitR, LitRed, LitDiffuseRed:
					var v uint64
					v, err = strconv.ParseUint(values[i], 10, 8)
					if err == nil {
						vertex.R = uint8(v)
					}
				case LitG, LitGreen, LitDiffuseGreen:
					var v uint64
					v, err = strconv.ParseUint(values[i], 10, 8)
					if err == nil {
						vertex.G = uint8(v)
					}
				case LitB, LitBlue, LitDiffuseBlue:
					var v uint64
					v, err = strconv.ParseUint(values[i], 10, 8)
					if err == nil {
						vertex.B = uint8(v)
					}
				}
			}

			// Number format error
			if err != nil {
				return err
			}

			mesh.AddVertex(vertex)

			// Don't read too much
			if int64(len(mesh.Vertices)) == numVertices {
				readingFaces = true
			}
		} else {
			// Start reading faces

			// Parsing vertices count per face
			var verticesPerFaceCount, verticesPerFaceCountParseError = strconv.ParseInt(values[0], 10, 32)
			if verticesPerFaceCountParseError != nil {
				return verticesPerFaceCountParseError
			}

			var v = make([]uint64, verticesPerFaceCount)
			for i := 1; i <= int(verticesPerFaceCount); i++ {
				v[i-1], err = strconv.ParseUint(values[i], 10, 32)
				if err != nil {
					return err
				}
			}

			face := types.Face{VerticesCount: uint8(verticesPerFaceCount), HalfEdge: nil}

			var halfEdges = make([]*types.HalfEdge, verticesPerFaceCount)

			for i := uint8(0); i < face.VerticesCount; i++ {
				id := v[i] + v[(i+1)%face.VerticesCount]*math.MaxUint32
				halfEdge := types.HalfEdge{ID: id, Vertex: &mesh.Vertices[v[(i+1)%face.VerticesCount]], Opposite: nil, Face: &face, Next: nil}
				face.HalfEdge = &halfEdge
				halfEdgesMap[id] = &halfEdge
				halfEdges[i] = &halfEdge

				// Take Opposite
				opposite := halfEdgesMap[v[i]*math.MaxUint32+v[(i+1)%face.VerticesCount]]
				if opposite != nil {
					opposite.Opposite = &halfEdge
					halfEdge.Opposite = opposite
				}
			}

			// Set Nexts correctly
			for i := 0; i < len(halfEdges); i++ {
				halfEdges[i].Next = halfEdges[(i+1)%len(halfEdges)]
			}

			mesh.AddFace(face)
		}
	}

	return err
}

// read binary ply
func readPlyBinary(mesh *types.Mesh, reader *bufio.Reader, numVertices int64, numFaces int64, endianity binary.ByteOrder, elements []element) error {
	var err error
	var halfEdgesMap = make(map[uint64]*types.HalfEdge)

	for pt := int64(0); pt < numVertices; pt++ {
		var p types.Vertex
		p.Nx, p.Ny, p.Nz = -2, -2, -2

		for i := 0; i < len(elements); i++ {
			e := elements[i]
			switch e.name {
			case "x":
				switch e.typename {
				case LitDouble:
					binary.Read(reader, endianity, &p.X)
				case LitFloat:
					var v float32
					binary.Read(reader, endianity, &v)
					p.X = float64(v)
				}
			case "y":
				switch e.typename {
				case LitDouble:
					binary.Read(reader, endianity, &p.Y)
				case LitFloat:
					var v float32
					binary.Read(reader, endianity, &v)
					p.Y = float64(v)
				}
			case "z":
				switch e.typename {
				case LitDouble:
					binary.Read(reader, endianity, &p.Z)
				case LitFloat:
					var v float32
					binary.Read(reader, endianity, &v)
					p.Z = float64(v)
				}

			case "nx":
				binary.Read(reader, endianity, &p.Nx)
			case "ny":
				binary.Read(reader, endianity, &p.Ny)
			case "nz":
				binary.Read(reader, endianity, &p.Nz)

			case LitR, LitRed, LitDiffuseRed:
				binary.Read(reader, endianity, &p.R)
			case LitG, LitGreen, LitDiffuseGreen:
				binary.Read(reader, endianity, &p.G)
			case LitB, LitBlue, LitDiffuseBlue:
				binary.Read(reader, endianity, &p.B)

			default:
				switch e.typename {
				case LitFloat:
					reader.Discard(4)
				case LitDouble:
					reader.Discard(8)
				case "uchar", "char", "byte":
					reader.Discard(1)
				case "int", "uint":
					reader.Discard(4)
				}
			}
		}

		mesh.AddVertex(p)
	}

	for pt := int64(0); pt < numFaces; pt++ {

		face := types.Face{VerticesCount: 0, HalfEdge: nil}
		binary.Read(reader, endianity, &face.VerticesCount)

		var v = make([]uint32, face.VerticesCount)
		for i := uint8(0); i < face.VerticesCount; i++ {
			binary.Read(reader, endianity, &v[i])
		}

		var halfEdges = make([]*types.HalfEdge, face.VerticesCount)

		for i := uint8(0); i < face.VerticesCount; i++ {
			id := uint64(v[i]) + uint64(v[(i+1)%face.VerticesCount])*math.MaxUint32
			halfEdge := types.HalfEdge{ID: id, Vertex: &mesh.Vertices[v[(i+1)%face.VerticesCount]], Opposite: nil, Face: &face, Next: nil}
			face.HalfEdge = &halfEdge
			halfEdgesMap[id] = &halfEdge
			halfEdges[i] = &halfEdge

			// Take Opposite
			opposite := halfEdgesMap[uint64(v[i])*math.MaxUint32+uint64(v[(i+1)%face.VerticesCount])]
			if opposite != nil {
				opposite.Opposite = &halfEdge
				halfEdge.Opposite = opposite
			}
		}

		// Set Nexts correctly
		for i := 0; i < len(halfEdges); i++ {
			halfEdges[i].Next = halfEdges[(i+1)%len(halfEdges)]
		}

		mesh.AddFace(face)
	}

	return err
}
