package io

import (
	"fmt"
	"path/filepath"

	"bitbucket.org/3domfbk/meshtools/types"
)

// ReadMesh reads a mesh from the specified file
func ReadMesh(filename string) (*types.Mesh, error) {
	extension := filepath.Ext(filename)
	switch extension {
	case ".ply":
		return readPly(filename)
	}

	return nil, fmt.Errorf("Format not supported: %s", extension)
}
