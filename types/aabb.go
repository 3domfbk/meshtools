package types

import "math"

// AABB is the Axis Aligned Bounding Box of the Faces
type AABB struct {
	Faces                              []*Face
	MinX, MinY, MinZ, MaxX, MaxY, MaxZ float64
}

// NewAABB creates an AABB
func NewAABB(face *Face) AABB {
	boundingBox := AABB{[]*Face{face}, math.MaxFloat64, math.MaxFloat64, math.MaxFloat64, -math.MaxFloat64, -math.MaxFloat64, -math.MaxFloat64}

	it := face.HalfEdge
	for i := uint8(0); i < face.VerticesCount; i++ {
		boundingBox.MinX = math.Min(boundingBox.MinX, it.Vertex.X)
		boundingBox.MinY = math.Min(boundingBox.MinY, it.Vertex.Y)
		boundingBox.MinZ = math.Min(boundingBox.MinZ, it.Vertex.Z)
		boundingBox.MaxX = math.Max(boundingBox.MaxX, it.Vertex.X)
		boundingBox.MaxY = math.Max(boundingBox.MaxY, it.Vertex.Y)
		boundingBox.MaxZ = math.Max(boundingBox.MaxZ, it.Vertex.Z)
		it = it.Next
	}

	return boundingBox
}

// Merge merges two AABB
func Merge(current, other AABB) AABB {
	boundingBox := AABB{make([]*Face, len(current.Faces)+len(other.Faces)), math.Min(current.MinX, other.MinX), math.Min(current.MinY, other.MinY), math.Min(current.MinZ, other.MinZ), math.Max(current.MaxX, other.MaxX), math.Max(current.MaxY, other.MaxY), math.Max(current.MaxZ, other.MaxZ)}

	for i := 0; i < len(current.Faces); i++ {
		boundingBox.Faces[i] = current.Faces[i]
	}

	for i := 0; i < len(other.Faces); i++ {
		boundingBox.Faces[len(current.Faces)+i] = other.Faces[i]
	}

	return boundingBox
}
