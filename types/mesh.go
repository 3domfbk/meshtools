package types

// Vertex of a mesh face
type Vertex struct {
	X, Y, Z    float64
	Nx, Ny, Nz float32
	R, G, B    uint8
}

// HalfEdge data structure
type HalfEdge struct {
	ID       uint64
	Vertex   *Vertex   // vertex at the end of the half-edge
	Opposite *HalfEdge // oppositely oriented adjacent half-edge
	Face     *Face     // face the half-edge borders
	Next     *HalfEdge // next half-edge around the face
}

// Face data structure
type Face struct {
	VerticesCount uint8
	HalfEdge      *HalfEdge // one of the half-edges bordering the face
}

// Mesh data structure
type Mesh struct {
	Vertices              []Vertex
	Faces                 []Face
	HasNormals, HasColors bool
}

// NewMesh constructor with default capacity
func NewMesh(vertices, faces int64, hasNormals, hasColors bool) *Mesh {
	return &Mesh{make([]Vertex, 0, vertices), make([]Face, 0, faces), hasNormals, hasColors}
}

// AddVertex to the mesh
func (mesh *Mesh) AddVertex(v Vertex) {
	mesh.Vertices = append(mesh.Vertices, v)
}

// AddFace to the mesh
func (mesh *Mesh) AddFace(f Face) {
	mesh.Faces = append(mesh.Faces, f)
}
