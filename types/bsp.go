package types

// AABBTree is a BSP-tree
type AABBTree struct {
	Root *AABBTreeNode
}

// AABBTreeNode of a BSP-tree containing an AABB
type AABBTreeNode struct {
	BoundingBox AABB
	Left        *AABBTreeNode
	Right       *AABBTreeNode
}

// IsLeaf checks if a node is a leaf node
func (node *AABBTreeNode) IsLeaf() bool {
	return len(node.BoundingBox.Faces) == 1 && (node.Left == nil) && (node.Right == nil)
}
