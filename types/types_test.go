package types

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestNewAABB(t *testing.T) {
	Convey("Given a face, computes the AABB", t, func() {
		v1 := Vertex{X: -11.0, Y: 2.0, Z: 4.0}
		v2 := Vertex{X: -5.0, Y: -1.0, Z: 2.0}
		v3 := Vertex{X: 1.0, Y: 3.0, Z: -7.0}

		he1 := HalfEdge{Vertex: &v1, Opposite: nil, Face: nil, Next: nil}
		he2 := HalfEdge{Vertex: &v2, Opposite: nil, Face: nil, Next: nil}
		he3 := HalfEdge{Vertex: &v3, Opposite: nil, Face: nil, Next: &he1}

		he1.Next = &he2
		he2.Next = &he3

		face := Face{VerticesCount: 3, HalfEdge: &he1}

		aabb := NewAABB(&face)

		So(face.HalfEdge.Next.Next.Next, ShouldEqual, face.HalfEdge)
		So(aabb.Faces, ShouldHaveLength, 1)
		So(aabb.MinX, ShouldEqual, -11.0)
		So(aabb.MinY, ShouldEqual, -1.0)
		So(aabb.MinZ, ShouldEqual, -7.0)
		So(aabb.MaxX, ShouldEqual, 1.0)
		So(aabb.MaxY, ShouldEqual, 3.0)
		So(aabb.MaxZ, ShouldEqual, 4.0)
	})
}

func TestMergeAABB(t *testing.T) {
	Convey("Given a face, computes the AABB", t, func() {
		// First face
		va1 := Vertex{X: -11.0, Y: 2.0, Z: 4.0}
		va2 := Vertex{X: -5.0, Y: -1.0, Z: 2.0}
		va3 := Vertex{X: 1.0, Y: 3.0, Z: -7.0}

		hea1 := HalfEdge{Vertex: &va1, Opposite: nil, Face: nil, Next: nil}
		hea2 := HalfEdge{Vertex: &va2, Opposite: nil, Face: nil, Next: nil}
		hea3 := HalfEdge{Vertex: &va3, Opposite: nil, Face: nil, Next: &hea1}

		hea1.Next = &hea2
		hea2.Next = &hea3

		faceA := Face{VerticesCount: 3, HalfEdge: &hea1}

		So(faceA.HalfEdge.Next.Next.Next, ShouldEqual, faceA.HalfEdge)

		aabbA := NewAABB(&faceA)

		So(aabbA.Faces, ShouldHaveLength, 1)
		So(aabbA.MinX, ShouldEqual, -11.0)
		So(aabbA.MinY, ShouldEqual, -1.0)
		So(aabbA.MinZ, ShouldEqual, -7.0)
		So(aabbA.MaxX, ShouldEqual, 1.0)
		So(aabbA.MaxY, ShouldEqual, 3.0)
		So(aabbA.MaxZ, ShouldEqual, 4.0)

		// Second face

		vb1 := Vertex{X: -6.0, Y: -2.0, Z: 5.0}
		vb2 := Vertex{X: -8.0, Y: -3.0, Z: 3.0}
		vb3 := Vertex{X: 2.0, Y: 7.0, Z: -1.0}

		heb1 := HalfEdge{Vertex: &vb1, Opposite: nil, Face: nil, Next: nil}
		heb2 := HalfEdge{Vertex: &vb2, Opposite: nil, Face: nil, Next: nil}
		heb3 := HalfEdge{Vertex: &vb3, Opposite: nil, Face: nil, Next: &heb1}

		heb1.Next = &heb2
		heb2.Next = &heb3

		faceB := Face{VerticesCount: 3, HalfEdge: &heb1}

		aabbB := NewAABB(&faceB)

		So(aabbB.Faces, ShouldHaveLength, 1)
		So(aabbB.MinX, ShouldEqual, -8.0)
		So(aabbB.MinY, ShouldEqual, -3.0)
		So(aabbB.MinZ, ShouldEqual, -1.0)
		So(aabbB.MaxX, ShouldEqual, 2.0)
		So(aabbB.MaxY, ShouldEqual, 7.0)
		So(aabbB.MaxZ, ShouldEqual, 5.0)

		merged := Merge(aabbA, aabbB)

		So(merged.Faces, ShouldHaveLength, 2)

		So(merged.MinX, ShouldEqual, -11.0)
		So(merged.MinY, ShouldEqual, -3.0)
		So(merged.MinZ, ShouldEqual, -7.0)
		So(merged.MaxX, ShouldEqual, 2.0)
		So(merged.MaxY, ShouldEqual, 7.0)
		So(merged.MaxZ, ShouldEqual, 5.0)
	})
}
